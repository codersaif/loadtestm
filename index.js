#!/usr/bin/env node
/**
 * Created by saif on 7/17/2016.
 */

const program = require('commander');

program
    .arguments('<file>')
    .option('-u, --username <username>', 'The user to authenticate as')
    .option('-p, --password <password>', 'The user\'s password')
    .action(file => {
        console.log('user: %s pass: %s file: %s', program.username, program.password, file);
    })
    .parse(process.argv);
